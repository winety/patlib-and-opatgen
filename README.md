# PATLIB and OPATGEN
These are the sources for [PATLIB and OPATGEN](https://web.archive.org/web/20050223203701/http://www.fi.muni.cz/~xantos/patlib/). Originally written by David Antoš these source files were almost lost. I'm reuploading them here mainly for archival purposes.

## License
Copyright 2001 David Antoš

You may use and redistribute this software under the terms of General Public License. As this software is distributed free of charge, there is no warranty for the program. The entire rist of this program is with you.

The author does not want to forbid anyone to use this software, nevertheless the author considers any military usage unmoral and unethical.
